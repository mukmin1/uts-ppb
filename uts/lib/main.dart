import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Biodata Diri',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Biodata(),
    );
  }
}

class Biodata extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.brown[700],
        title: Center(
          child: Text(
            'Profil Mahasiswa',
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.all(10),
            height: 1000,
            width: 700,
            decoration: (BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: Colors.brown,
            )),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50),
                    child: Image(
                      image: AssetImage('images/profil.jpg'),
                    ),
                  ),
                Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(),
                          margin: EdgeInsets.only(bottom: 15, top: 25),
                          height: 30,
                          width: 350,
                          child: Text('BIODATA MAHASISWA',
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'NPM                       :       031200014',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'Nama                     :       Nurul Mukmin',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'Prodi                      :       D3 Sistem Informasi',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'No HP                    :      088275666648',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'TTL                        :       Palembang,06 mei 1993',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          padding: EdgeInsets.only(left: 40),
                          child: Text('Jenis Kelamin      :       Laki-laki',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          padding: EdgeInsets.only(left: 40),
                          child: Text('Agama                  :       Islam',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ],
                    ))
              ],
            )),
      ),
    );
  }
}
